#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>


int main(int argc, char** argv)
{
	// 将域名解析为 IP 地址
	struct hostent* hi = gethostbyname("news.baidu.com");

	char* ip = inet_ntoa(*(struct in_addr*)hi->h_addr);

	//printf("%s\n", ip);

	int sock = socket(AF_INET, SOCK_STREAM, 0);

	if(sock == -1)
	{
		perror("socket");
		return 1;
	}

	struct sockaddr_in srv_addr;
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr(ip);
	srv_addr.sin_port = htons(80);

	if(connect(sock, (struct sockaddr*)&srv_addr, sizeof(srv_addr)) == -1)
	{
		perror("connect");
		return 1;
	}

	int ret, cnt = 0;
	char request[] = "GET / HTTP/1.1\r\nHost: news.baidu.com\r\nConnection: close\r\n\r\n";
	char* response = (char*)calloc(1, 3 * 1024 * 1024);

	send(sock, request, strlen(request), 0);

	while((ret = recv(sock, response + cnt, 3 * 1024 * 1024 - cnt - 1, 0)) > 0)
	{
		cnt += ret;
	}
	
	//printf("%s\n", response);
	
	char* p = NULL, *q = NULL, *r = NULL, *s = NULL;
	
	p = strstr(response, "<ul class=\"hotwords clearfix\">");

	if(p != NULL)
	{
		q = strstr(p, "</ul>");

		if(q != NULL)
		{
			*q = '\0';
			//printf("%s\n", p);

			while(r = strstr(p, "title=\""))
			{
				r += 7;
				s = strstr(r, "\"");
				*s = '\0';
			
				printf("%s\n", r);

				p = s + 1;
			}
		}
	}

	free(response);
		
	close(sock);

	return 0;
}
